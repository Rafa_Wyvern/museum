VRChatのワールド制作をgitで行うワークフローを模索するためのプロジェクトです。
プロジェクトはMITライセンスでオープンソースとするため、MITライセンスに同意できない場合はコミットしないでください。

再配布不可能なアセットはignoreされています。cloneした後、実行前にVRC SDKは手元で入れ直して作業してください。

その他、実行に必要なアセットは以下

* Free HDR Sky https://assetstore.unity.com/packages/2d/textures-materials/sky/free-hdr-sky-61217
* ProBuilder https://assetstore.unity.com/packages/tools/modeling/probuilder-111418